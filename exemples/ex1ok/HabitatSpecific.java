import java.util.ArrayList;

public class HabitatSpecific extends Habitat {

public HabitatSpecific(ArrayList<CAppareil> lapp, ArrayList<CEnsAppareil> lens, ArrayList<CScenario> lscen, ArrayList<CInterface> lint, ArrayList<CProgrammation> lprog)
{
super(lapp,lens,lscen,lint,lprog);
}
public void execScenarioNum(int num) 
{
System.out.println( "Execution du scenario "+this.l_scenarios.get(num).getNomScenario()+"... ");
	switch(num) {

case 0:
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("e1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("e1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("e2"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("v1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.OUVERT))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("v1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.FERME))
appareil.appliquer(TypeActionAppareil.OUVRIR_PARTIEL); System.out.println(" alors ");
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("v1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))
appareil.appliquer(TypeActionAppareil.FERMER);appareil.appliquer(TypeActionAppareil.OUVRIR); System.out.println(" sinon ");

break;

case 1:
 System.out.println("Etat de ""+appareil.getNomAppareil()+"" = "e.etat);

for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.AUTRE_APPAREIL_ORDINATEUR))appareil.appliquer(TypeActionAppareil.ALLUMER);
this.execScenarioNum(1);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("a1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("a1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("a1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
{ for(CAppareil appareil2 : this.l_appareils)
if (appareil2.getNomAppareil().equals("a2"))
appareil2.appliquer(TypeActionAppareil.ALLUMER); }
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("r1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ECO))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("r1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("r1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
appareil.appliquer(TypeActionAppareil.ALLUMER_ECO);appareil.appliquer(TypeActionAppareil.ETEINDRE); System.out.println(" bip !");
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("fen"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.OUVERT))
 System.out.println("fenêtre ouverte !");
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("cafe"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("cafe"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))

break;

case 2:
this.execScenarioNum(1);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("hf"))  appareil.appliquer(TypeActionAppareil.ALLUMER);

break;

case 3:
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.FERMER);
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.ECLAIRAGE))appareil.appliquer(TypeActionAppareil.ETEINDRE);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("fen"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.FERME))
{ for(CAppareil appareil2 : this.l_appareils)
if (appareil2.getNomAppareil().equals("a1"))
appareil2.appliquer(TypeActionAppareil.ALLUMER); }
else  System.out.println("Attention : la fenêtre ""+appareil.getNomAppareil()+"" est ouverte !");
for(CAppareil appareil : this.l_appareils)
if (appareil.estTypeAutreAppareil())appareil.appliquer(TypeActionAppareil.ETEINDRE);

break;

case 4:
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.OUVRIR);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("a1"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("cafe"))  appareil.appliquer(TypeActionAppareil.ALLUMER);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("rad1"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("hf"))  appareil.appliquer(TypeActionAppareil.ALLUMER);

break;

case 5:
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.FERMER);
for(CEnsAppareil ensAppareil : this.l_ensembles)if (ensAppareil.nomEnsAppareil.equals("mon_eclairage_salon"))for(CAppareil appareil : ensAppareil.lAppareils)appareil.appliquer(TypeActionAppareil.ALLUMER);

break;
default:
}
}
}
