public class CMaisonUser extends CMaison {

public CMaisonUser() {

super();


// Appareils
CVoletFenetre v1 = new CVoletFenetre ("v1",TypeAppareil.VOLET);
ma_liste_appareils.add(v1);
CVoletFenetre v2 = new CVoletFenetre ("v2",TypeAppareil.VOLET);
ma_liste_appareils.add(v2);
CVoletFenetre v3 = new CVoletFenetre ("v3",TypeAppareil.VOLET);
ma_liste_appareils.add(v3);
CVoletFenetre v4 = new CVoletFenetre ("v4",TypeAppareil.VOLET);
ma_liste_appareils.add(v4);
CVoletFenetre v5 = new CVoletFenetre ("v5",TypeAppareil.VOLET);
ma_liste_appareils.add(v5);
CAlarme a1 = new CAlarme("a1",TypeAppareil.ALARME);
ma_liste_appareils.add(a1);
CAlarme a2 = new CAlarme("a2",TypeAppareil.ALARME);
ma_liste_appareils.add(a2);
CAlarme a3 = new CAlarme("a3",TypeAppareil.ALARME);
ma_liste_appareils.add(a3);
CVoletFenetre fen = new CVoletFenetre ("fen",TypeAppareil.FENETRE);
ma_liste_appareils.add(fen);
CChauffage r1 = new CChauffage("r1",TypeAppareil.CHAUFFAGE);
ma_liste_appareils.add(r1);
CChauffage rad1 = new CChauffage("rad1",TypeAppareil.CHAUFFAGE);
ma_liste_appareils.add(rad1);
CEclairage e1 = new CEclairage("e1",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e1);
CEclairage e2 = new CEclairage("e2",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e2);
CEclairage e3 = new CEclairage("e3",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e3);
CAutreAppareil matv = new CAutreAppareil("matv",TypeAppareil.AUTRE_APPAREIL_TV);
ma_liste_appareils.add(matv);
CAutreAppareil lv = new CAutreAppareil("lv",TypeAppareil.AUTRE_APPAREIL_LAVE_VAISSELLE);
ma_liste_appareils.add(lv);
CAutreAppareil port = new CAutreAppareil("port",TypeAppareil.AUTRE_APPAREIL_PORTAIL);
ma_liste_appareils.add(port);
CAutreAppareil hf = new CAutreAppareil("hf",TypeAppareil.AUTRE_APPAREIL_HIFI);
ma_liste_appareils.add(hf);
CAutreAppareil cafe = new CAutreAppareil("cafe",TypeAppareil.AUTRE_APPAREIL_CAFETIERE);
ma_liste_appareils.add(cafe);
CAutreAppareil ll = new CAutreAppareil("ll",TypeAppareil.AUTRE_APPAREIL_LAVE_LINGE);
ma_liste_appareils.add(ll);
CAutreAppareil proj = new CAutreAppareil("proj",TypeAppareil.AUTRE_APPAREIL_VIDEO_PROJ);
ma_liste_appareils.add(proj);
CAutreAppareil ordi = new CAutreAppareil("ordi",TypeAppareil.AUTRE_APPAREIL_ORDINATEUR);
ma_liste_appareils.add(ordi);
CAutreAppareil ordi1 = new CAutreAppareil("ordi1",TypeAppareil.AUTRE_APPAREIL_ORDINATEUR);
ma_liste_appareils.add(ordi1);
CAutreAppareil ordi2 = new CAutreAppareil("ordi2",TypeAppareil.AUTRE_APPAREIL_ORDINATEUR);
ma_liste_appareils.add(ordi2);

// Ensembles d'appareils
CEnsAppareil elec_salon = new CEnsAppareil("elec_salon");
elec_salon.addAppareil(matv);
elec_salon.addAppareil(proj);
elec_salon.addAppareil(ordi);
ma_liste_ens_appareils.add(elec_salon);
CEnsAppareil mon_eclairage_salon = new CEnsAppareil("mon_eclairage_salon");
mon_eclairage_salon.addAppareil(e2);
mon_eclairage_salon.addAppareil(e3);
ma_liste_ens_appareils.add(mon_eclairage_salon);

// Interfaces
CInterface b1 = new CInterface("b1",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b1);
CInterface b2 = new CInterface("b2",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b2);
CInterface tab1 = new CInterface("tab1",TypeInterface.TABLETTE);
ma_liste_interfaces.add(tab1);
CInterface t1 = new CInterface("t1",TypeInterface.MOBILE);
ma_liste_interfaces.add(t1);
CInterface tel1 = new CInterface("tel1",TypeInterface.TELEPHONE);
ma_liste_interfaces.add(tel1);
CInterface zap = new CInterface("zap",TypeInterface.TELECOMMANDE);
ma_liste_interfaces.add(zap);

// Scenarios
String test2_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e2\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.OUVERT))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.FERME))\n"+
"appareil.appliquer(TypeActionAppareil.OUVRIR_PARTIEL); System.out.println(\" alors \");\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))\n"+
"appareil.appliquer(TypeActionAppareil.FERMER);appareil.appliquer(TypeActionAppareil.OUVRIR); System.out.println(\" sinon \");";
CScenario test2 = new CScenario("test2",test2_contenu);
ma_liste_scenarios.add(test2);

String test_contenu = "\n System.out.println(\"Etat de \"\"+appareil.getNomAppareil()+\"\" = \"e.etat);\n"+
"\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.AUTRE_APPAREIL_ORDINATEUR))appareil.appliquer(TypeActionAppareil.ALLUMER);\n"+
"this.execScenarioNum(1);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"a1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"a1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.DEMI))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"a1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
"{ for(CAppareil appareil2 : this.l_appareils)\n"+
"if (appareil2.getNomAppareil().equals(\"a2\"))\n"+
"appareil2.appliquer(TypeActionAppareil.ALLUMER); }\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"r1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ECO))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"r1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"r1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
"appareil.appliquer(TypeActionAppareil.ALLUMER_ECO);appareil.appliquer(TypeActionAppareil.ETEINDRE); System.out.println(\" bip !\");\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"fen\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.OUVERT))\n"+
" System.out.println(\"fenêtre ouverte !\");\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"cafe\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"cafe\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))";
CScenario test = new CScenario("test",test_contenu);
ma_liste_scenarios.add(test);

String soiree_musique_contenu = "\nthis.execScenarioNum(1);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"hf\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario soiree_musique = new CScenario("soiree_musique",soiree_musique_contenu);
ma_liste_scenarios.add(soiree_musique);

String depart_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.FERMER);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.ECLAIRAGE))appareil.appliquer(TypeActionAppareil.ETEINDRE);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"fen\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.FERME))\n"+
"{ for(CAppareil appareil2 : this.l_appareils)\n"+
"if (appareil2.getNomAppareil().equals(\"a1\"))\n"+
"appareil2.appliquer(TypeActionAppareil.ALLUMER); }\n"+
"else  System.out.println(\"Attention : la fenêtre \"\"+appareil.getNomAppareil()+\"\" est ouverte !\");\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.estTypeAutreAppareil())appareil.appliquer(TypeActionAppareil.ETEINDRE);";
CScenario depart = new CScenario("depart",depart_contenu);
ma_liste_scenarios.add(depart);

String bonjour_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.OUVRIR);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"a1\"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"cafe\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"rad1\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ETEINT))\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"hf\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario bonjour = new CScenario("bonjour",bonjour_contenu);
ma_liste_scenarios.add(bonjour);

String soiree_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.FERMER);\n"+
"for(CEnsAppareil ensAppareil : this.l_ensembles)if (ensAppareil.nomEnsAppareil.equals(\"mon_eclairage_salon\"))for(CAppareil appareil : ensAppareil.lAppareils)appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario soiree = new CScenario("soiree",soiree_contenu);
ma_liste_scenarios.add(soiree);


// Commandes association
tel1.addScenarioAssocie("test");
b2.addScenarioAssocie("depart");
zap.addScenarioAssocie("test");
tab1.addScenarioAssocie("test");
t1.addScenarioAssocie("depart");
b1.addScenarioAssocie("bonjour");

// Commandes programmation
CProgrammation p1 = new CProgrammation("test2");
CDate p1d1 = new CDate(-1,1,1,6,0);
p1.addDate(p1d1);
ma_liste_programmations.add(p1);
CProgrammation p2 = new CProgrammation("soiree_musique");
CDate p2d1 = new CDate(2012,11,20,19,30);
p2.addDate(p2d1);
CDate p2d2 = new CDate(2012,11,21,19,30);
p2.addDate(p2d2);
ma_liste_programmations.add(p2);
CProgrammation p3 = new CProgrammation("soiree");
CDate p3d1 = new CDate(2012,-1,1,18,0);
p3.addDate(p3d1);
ma_liste_programmations.add(p3);

monHabitat = new HabitatSpecific(ma_liste_appareils,ma_liste_ens_appareils, ma_liste_scenarios,ma_liste_interfaces, ma_liste_programmations);
}
}