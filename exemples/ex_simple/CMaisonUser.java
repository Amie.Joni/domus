public class CMaisonUser extends CMaison {

public CMaisonUser() {

super();


// Appareils
CVoletFenetre v1 = new CVoletFenetre ("v1",TypeAppareil.VOLET);
ma_liste_appareils.add(v1);
CVoletFenetre v2 = new CVoletFenetre ("v2",TypeAppareil.VOLET);
ma_liste_appareils.add(v2);
CEclairage e1 = new CEclairage("e1",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e1);
CEclairage e2 = new CEclairage("e2",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e2);
CAutreAppareil hf = new CAutreAppareil("hf",TypeAppareil.AUTRE_APPAREIL_HIFI);
ma_liste_appareils.add(hf);
CAutreAppareil cafe = new CAutreAppareil("cafe",TypeAppareil.AUTRE_APPAREIL_CAFETIERE);
ma_liste_appareils.add(cafe);

// Ensembles d'appareils
CEnsAppareil mon_eclairage_salon = new CEnsAppareil("mon_eclairage_salon");
mon_eclairage_salon.addAppareil(e1);
mon_eclairage_salon.addAppareil(e2);
ma_liste_ens_appareils.add(mon_eclairage_salon);

// Interfaces
CInterface b1 = new CInterface("b1",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b1);
CInterface b2 = new CInterface("b2",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b2);
CInterface b3 = new CInterface("b3",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b3);
CInterface b4 = new CInterface("b4",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b4);
CInterface t1 = new CInterface("t1",TypeInterface.MOBILE);
ma_liste_interfaces.add(t1);
CInterface zap = new CInterface("zap",TypeInterface.TELECOMMANDE);
ma_liste_interfaces.add(zap);

// Scenarios
String bonjour2_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.OUVRIR);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e1\"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.estTypeAutreAppareil())appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario bonjour2 = new CScenario("bonjour2",bonjour2_contenu);
ma_liste_scenarios.add(bonjour2);

String soiree_musique_contenu = "\nthis.execScenarioNum(1);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"hf\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
" System.out.println(\"La hifi est deja allumee !\");";
CScenario soiree_musique = new CScenario("soiree_musique",soiree_musique_contenu);
ma_liste_scenarios.add(soiree_musique);

String fincafe_contenu = "\n System.out.println(\"Extinction de la machine a cafe.\");\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.typeAppareil.equals(TypeAppareil.AUTRE_APPAREIL_CAFETIERE))appareil.appliquer(TypeActionAppareil.ETEINDRE);";
CScenario fincafe = new CScenario("fincafe",fincafe_contenu);
ma_liste_scenarios.add(fincafe);

String verif_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
" System.out.println(\"Etat de \"\"+appareil.getNomAppareil()+\"\" = \"e.etat);\n"+
"\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"hf\"))\n"+
"if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))\n"+
" System.out.println(\"La hifi est allumee.\");";
CScenario verif = new CScenario("verif",verif_contenu);
ma_liste_scenarios.add(verif);

String bonjour_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v1\"))  appareil.appliquer(TypeActionAppareil.OUVRIR);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v2\"))  appareil.appliquer(TypeActionAppareil.OUVRIR);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e1\"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"hf\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"cafe\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario bonjour = new CScenario("bonjour",bonjour_contenu);
ma_liste_scenarios.add(bonjour);

String soiree_contenu = "\nfor(CEnsAppareil ensAppareil : this.l_ensembles)if (ensAppareil.nomEnsAppareil.equals(\"mon_eclairage_salon\"))for(CAppareil appareil : ensAppareil.lAppareils)appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario soiree = new CScenario("soiree",soiree_contenu);
ma_liste_scenarios.add(soiree);


// Commandes association
b2.addScenarioAssocie("bonjour2");
b3.addScenarioAssocie("soiree");
b3.addScenarioAssocie("fincafe");
b3.addScenarioAssocie("verif");
b4.addScenarioAssocie("soiree_musique");
zap.addScenarioAssocie("bonjour");
t1.addScenarioAssocie("bonjour");
b1.addScenarioAssocie("bonjour");

// Commandes programmation
CProgrammation p1 = new CProgrammation("soiree_musique");
CDate p1d1 = new CDate(2017,12,31,19,0);
p1.addDate(p1d1);
CDate p1d2 = new CDate(2017,7,14,19,30);
p1.addDate(p1d2);
CDate p1d3 = new CDate(2017,12,24,18,30);
p1.addDate(p1d3);
ma_liste_programmations.add(p1);
CProgrammation p2 = new CProgrammation("soiree");
CDate p2d1 = new CDate(2017,-1,1,18,0);
p2.addDate(p2d1);
ma_liste_programmations.add(p2);

monHabitat = new HabitatSpecific(ma_liste_appareils,ma_liste_ens_appareils, ma_liste_scenarios,ma_liste_interfaces, ma_liste_programmations);
}
}