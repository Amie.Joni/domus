import java.util.ArrayList;

public class HabitatSpecific extends Habitat {

public HabitatSpecific(ArrayList<CAppareil> lapp, ArrayList<CEnsAppareil> lens, ArrayList<CScenario> lscen, ArrayList<CInterface> lint, ArrayList<CProgrammation> lprog)
{
super(lapp,lens,lscen,lint,lprog);
}
public void execScenarioNum(int num) 
{
System.out.println( "Execution du scenario "+this.l_scenarios.get(num).getNomScenario()+"... ");
	switch(num) {

case 0:
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.VOLET))appareil.appliquer(TypeActionAppareil.OUVRIR);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("e1"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);
for(CAppareil appareil : this.l_appareils)
if (appareil.estTypeAutreAppareil())appareil.appliquer(TypeActionAppareil.ALLUMER);

break;

case 1:
this.execScenarioNum(1);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("hf"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
 System.out.println("La hifi est deja allumee !");

break;

case 2:
 System.out.println("Extinction de la machine a cafe.");
for(CAppareil appareil : this.l_appareils)
if (appareil.typeAppareil.equals(TypeAppareil.AUTRE_APPAREIL_CAFETIERE))appareil.appliquer(TypeActionAppareil.ETEINDRE);

break;

case 3:
for(CAppareil appareil : this.l_appareils)
 System.out.println("Etat de ""+appareil.getNomAppareil()+"" = "e.etat);

for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("hf"))
if (appareil.etatAppareil.equals(TypeEtatAppareil.ALLUME))
 System.out.println("La hifi est allumee.");

break;

case 4:
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("v1"))  appareil.appliquer(TypeActionAppareil.OUVRIR);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("v2"))  appareil.appliquer(TypeActionAppareil.OUVRIR);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("e1"))  appareil.appliquer(TypeActionAppareil.ETEINDRE);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("hf"))  appareil.appliquer(TypeActionAppareil.ALLUMER);
for(CAppareil appareil : this.l_appareils)
if (appareil.getNomAppareil().equals("cafe"))  appareil.appliquer(TypeActionAppareil.ALLUMER);

break;

case 5:
for(CEnsAppareil ensAppareil : this.l_ensembles)if (ensAppareil.nomEnsAppareil.equals("mon_eclairage_salon"))for(CAppareil appareil : ensAppareil.lAppareils)appareil.appliquer(TypeActionAppareil.ALLUMER);

break;
default:
}
}
}
