public class CMaisonUser extends CMaison {

public CMaisonUser() {

super();


// Appareils
CVoletFenetre v1 = new CVoletFenetre ("v1",TypeAppareil.VOLET);
ma_liste_appareils.add(v1);
CEclairage e1 = new CEclairage("e1",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e1);
CEclairage e2 = new CEclairage("e2",TypeAppareil.ECLAIRAGE);
ma_liste_appareils.add(e2);

// Ensembles d'appareils

// Interfaces
CInterface b1 = new CInterface("b1",TypeInterface.INTERRUPTEUR);
ma_liste_interfaces.add(b1);
CInterface t1 = new CInterface("t1",TypeInterface.MOBILE);
ma_liste_interfaces.add(t1);
CInterface zap = new CInterface("zap",TypeInterface.TELECOMMANDE);
ma_liste_interfaces.add(zap);

// Scenarios
String bonjour_contenu = "\nfor(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"v1\"))  appareil.appliquer(TypeActionAppareil.OUVRIR);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e1\"))  appareil.appliquer(TypeActionAppareil.TAMISER);\n"+
"for(CAppareil appareil : this.l_appareils)\n"+
"if (appareil.getNomAppareil().equals(\"e2\"))  appareil.appliquer(TypeActionAppareil.ALLUMER);";
CScenario bonjour = new CScenario("bonjour",bonjour_contenu);
ma_liste_scenarios.add(bonjour);


// Commandes association
zap.addScenarioAssocie("bonjour");
t1.addScenarioAssocie("bonjour");
b1.addScenarioAssocie("bonjour");

// Commandes programmation

monHabitat = new HabitatSpecific(ma_liste_appareils,ma_liste_ens_appareils, ma_liste_scenarios,ma_liste_interfaces, ma_liste_programmations);
}
}