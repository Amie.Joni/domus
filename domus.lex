//Specification JFlex
import java_cup.runtime.Symbol;

%%
%unicode
%cup
%line
%column

%{
public int getYyLine(){return yyline+1;}
public int getYyColumn(){return yycolumn+1;}
public String getYyText(){return yytext();}
%}
nombre = [0-9]
%%

{nombre}+                   {return new Symbol( sym.NOMBRE, new Integer(yytext()));}
"fermer"                    {return new Symbol( sym.FERMER, yytext());}
"fermer_partiel"            {return new Symbol( sym.FERMEPAR, yytext());}
"ouvrir"                    {return new Symbol( sym.OUVRIR, yytext());}
"ouvrir_partiel"            {return new Symbol( sym.OUVRIRPAR, yytext());}
"allumer"                   {return new Symbol( sym.ALLUMER, yytext());}
"allumer_partiel"           {return new Symbol( sym.ALLUMEPAR, yytext());}
"allumer_eco"               {return new Symbol( sym.ALLUMEECO, yytext());}
"tamiser"                   {return new Symbol( sym.TAMISER, yytext());}
"eteindre"                  {return new Symbol( sym.ETEINDRE, yytext());}
"allume"                    {return new Symbol( sym.ALLUME, yytext());}
"eteint"                    {return new Symbol( sym.ETEINT, yytext());}
"demi"                      {return new Symbol( sym.DEMI, yytext());}
"ferme"                     {return new Symbol( sym.FERME, yytext());}
"eco"                       {return new Symbol( sym.ECO, yytext());}
"ouvert"                    {return new Symbol( sym.OUVERT, yytext());}
"associer"                  {return new Symbol( sym.ASSOCI,yytext() );}
"programmer"                {return new Symbol( sym.PROGRA,yytext() );}
"definir"                   {return new Symbol( sym.DEFINR,yytext() );}
"etat"                      {return new Symbol( sym.ETAT, yytext());}
"eclairage"                 {return new Symbol( sym.ECLAIR, yytext());}
"alarme"                    {return new Symbol( sym.ALARME,yytext() );}
"chauffage"                 {return new Symbol( sym.CHAUFF,yytext() );}
"fenetre"                   {return new Symbol( sym.FENETR,yytext() );}
"volet"                     {return new Symbol( sym.VOLET,yytext() );}
"autre_appareil"            {return new Symbol( sym.AU_APP,yytext() );}
"interrupteur"              {return new Symbol( sym.INTERR,yytext() );}
"tablette"          	    {return new Symbol( sym.TABLET,yytext() );}
"telephone"          	    {return new Symbol( sym.TELEPH,yytext() );}
"mobile"                    {return new Symbol( sym.MOBILE,yytext() );}
"telecommande"              {return new Symbol( sym.TELECO,yytext() );}
"tv"              	    {return new Symbol( sym.TV,yytext() );}
"hifi"              	    {return new Symbol( sym.HIFI,yytext() );}
"cafetiere"                 {return new Symbol( sym.CAFETIERE,yytext() );}
"video_proj"                {return new Symbol( sym.VIDEOPROJ,yytext() );}
"lave_vaisselle"            {return new Symbol( sym.LAVEVAISS,yytext() );}
"lave_linge"                {return new Symbol( sym.LAVELINGE,yytext() );}
"ordinateur"                {return new Symbol( sym.ORDINATEUR,yytext() );}
"portail"                   {return new Symbol( sym.PORTAIL,yytext() );}
"pourtout"                  {return new Symbol( sym.POURTO,yytext() );}
"faire"                     {return new Symbol( sym.FAIRE,yytext() );}
"fait"                      {return new Symbol( sym.FAIT,yytext() );}
"message"                   {return new Symbol( sym.MESSAG,yytext() );}
"executer_scenario"         {return new Symbol( sym.EXECUT,yytext() );}
"si"                        {return new Symbol( sym.SI,yytext() );}
"alors"                     {return new Symbol( sym.ALORS,yytext() );}
"sinon"                     {return new Symbol( sym.SINON,yytext() );}
"fsi"                       {return new Symbol( sym.FSI,yytext() );}
"<PROGRAMME_DOMUS>"           	{return new Symbol( sym.PROGRAMME_DOM_D,yytext() );}
"</PROGRAMME_DOMUS>"           	{return new Symbol( sym.PROGRAMME_DOM_F,yytext() );}
"<DECLARATION_APPAREILS>"   	{return new Symbol( sym.DECLARATION_APP_D,yytext() );}
"</DECLARATION_APPAREILS>"  	{return new Symbol( sym.DECLARATION_APP_F,yytext() );}
"<DECLARATION_INTERFACES>"    	{return new Symbol( sym.DECLARATION_INT_D,yytext() );}
"</DECLARATION_INTERFACES>"    	{return new Symbol( sym.DECLARATION_INT_F,yytext() );}
"<DECLARATION_SCENARII>"      	{return new Symbol( sym.DECLARATION_SCE_D,yytext() );}
"</DECLARATION_SCENARII>"      	{return new Symbol( sym.DECLARATION_SCE_F,yytext() );}
"<DECLARATION_COMMANDES>"     	{return new Symbol( sym.DECLARATION_COM_D,yytext() );}
"</DECLARATION_COMMANDES>"     	{return new Symbol( sym.DECLARATION_COM_F,yytext() );}
"<SCENARIO"                  	{return new Symbol( sym.SCENARIO_D,yytext() );}
"</SCENARIO"                  	{return new Symbol( sym.SCENARIO_F,yytext() );}
[a-zA-Z][a-zA-Z0-9_]*       {return new Symbol( sym.IDENT,yytext() );}
"_"                         {return new Symbol( sym.UNDERSCORE,yytext() );}
"\."                        {return new Symbol( sym.POINT,yytext() );}
"("                         {return new Symbol( sym.POUVR,yytext() );}
")"                         {return new Symbol( sym.PFERM,yytext() );}
";"                         {return new Symbol( sym.PVIRG,yytext() );}
":"                         {return new Symbol( sym.DPOINT,yytext() );}
"\,"                        {return new Symbol( sym.VIRGUL,yytext() );}
"{"                         {return new Symbol( sym.ACOUVR,yytext() );}
"}"                         {return new Symbol( sym.ACFERM,yytext() );}
"\>"                        {return new Symbol( sym.SUPERI,yytext() );}
"=="                        {return new Symbol( sym.EGUCMP,yytext() );}
"="                         {return new Symbol( sym.EGUAL,yytext() );}
\"[^\"\n]*\"                  {return new Symbol( sym.CHAINE,yytext() );}
\t|" "|\n|\/\/.*\n          {}
.                           {System.out.println( " [*] Erreur lexicale => ligne "+(yyline+1)+" colonne "+yycolumn+" caractere non reconnu :"+yytext() );}

